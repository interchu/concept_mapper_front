Introduction
============
This project is the frontend part of [omop-mapper.fgh.ovh](http://omop-mapper.fgh.ovh)

How to install ?
=================
Put WebContent in your web folder
Download the last release of [openUI5](https://openui5.org/releases/) in a folder called resources

Backend
=======
The backendpart is [here](https://framagit.org/interchu/omop-omop-back)