FROM nginx

RUN adduser --system app
USER app

ARG api_hostname
ARG api_port=5000

COPY concept_mapper_front/WebContent /usr/share/nginx/html
RUN apt update && apt install wget unzip -y

RUN echo $api_hostname
WORKDIR /usr/share/nginx/html/
# replace with the host private ip to allow selenium to access it from the browser
RUN     find . -type f -exec sed -i "s|http://localhost:5000/|http://$api_hostname:$api_port/|g" {} \;
RUN     wget https://github.com/SAP/openui5/releases/download/1.84.11/openui5-runtime-1.84.11.zip
RUN     unzip -o openui5-runtime-1.84.11.zip -d /tmp
RUN     mv /tmp/resources .
