sap.ui.define([], function() {
   "use strict";

   return {
	   
	   getUsers: function () {
	    	sap.ui.core.BusyIndicator.show(0);

  	 		var URL_users = "http://localhost:5000/users"
  			
  			if (sap.ui.getCore().getModel('loginModel') !== undefined) {
  				var token = sap.ui.getCore().getModel('loginModel').oData.access_token;
  			}
  			

  			$.ajax({
  				method:"GET",
  				url: URL_users,
  				dataType:"json",
  				async: false,
  				header: {
  					"Content-Type": "application/json",
  				},
  				beforeSend: function(xhr, settings) {  xhr.setRequestHeader('Authorization','Bearer ' + token); } ,
  				success: function(data, response, xhr) {

  					if (data.users) {
  						for (var i = 0; i < data.users.length; i++) {
  							var user = data.users[i];
  							
  							// DELETE actions
  							if (user.m_invalid_reason === 'D') {
  								user.m_invalid_reason = 'Invalid'
  							} else {
  								user.m_invalid_reason = 'Valid'

  							}
  						}
  					}
  					
  					
  					sap.ui.getCore().byId('oUserTable').setVisibleRowCount(data.users_number);

  					
  					var oModel = new sap.ui.model.json.JSONModel(data);
  					sap.ui.getCore().setModel(oModel, "usersModel");
  					console.log("Connection : getUsers()");
  				},
  				error : function(error) {
  					sap.m.MessageToast.show("Connexion error");
  					console.log("Connexion error : getUsers()");
  					console.log(error);
  				}
  			
  			});
	    	sap.ui.core.BusyIndicator.hide();

  	}



   };
});