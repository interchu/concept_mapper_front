
sap.ui.define([

], function() {
   "use strict";

   return {
	   
	   // find key code : https://api.jquery.com/keydown/
	   
	   initiationApp : function () {
		   

		   $(document).unbind('keydown');
		   
		   $(document).keydown(function(evt){
		   
				   if (evt.keyCode == 85 && (evt.ctrlKey) || evt.keyCode == 32 || evt.keyCode == 85 ) { // U key // space
				       evt.preventDefault();
				       sap.ui.controller("conceptmapper.searchPage").userAuthentification();
				   }
				 
		   });

		   
	   },
	   

	   

	   searchPage : function () {
		   
//			jQuery.sap.delayedCall(0, null, function() {
//				sap.ui.getCore().byId("searchField").focus();
//			});  
//			sap.ui.getCore().byId("searchField").focus();

		    $(document).unbind('keydown');
			$(document).keydown(function(evt){
				
				if (evt.keyCode == 70 && (evt.ctrlKey)) { // f key
			       evt.preventDefault();
			       sap.ui.getCore().byId("__container1-Master").toggleStyleClass("toggle-masterPage");
			    }

				else if (evt.keyCode == 75 && (evt.ctrlKey)) { // k key
					evt.preventDefault();

//					jQuery.sap.delayedCall(0, null, function() {
					sap.ui.getCore().byId("searchField").focus();
//					});    	    	 

				}
				
				else if (evt.keyCode == 77 && (evt.ctrlKey)) { // m key
					evt.preventDefault();

					sap.ui.controller("conceptmapper.searchPage").goToMapperPage();

				}
				
				else if (evt.keyCode == 37 && (evt.ctrlKey)&& sap.ui.getCore().byId('oButtonPrev').getEnabled()) { // left arrow
					sap.ui.controller("conceptmapper.searchPage").prevPage();
					
				}
				
				else if (evt.keyCode == 39 && (evt.ctrlKey) && sap.ui.getCore().byId('oButtonNext').getEnabled()) { // right arrow
					sap.ui.controller("conceptmapper.searchPage").nextPage();
				}
				   
				else if (evt.keyCode == 38 && (evt.ctrlKey) ) { // up arrow
					
					var idx = []
					var oTable_ref = sap.ui.getCore().byId('oTable');
					for (var i in oTable_ref.getAggregation('items')) {
						if (oTable_ref.getAggregation('items')[i].getAggregation('cells')[0].getProperty('selected')) {
							idx.push(i);
						}
					}
					
					if (idx.length === 0) {
						oTable_ref.getAggregation('items')[0].getAggregation('cells')[0].setProperty('selected', true);
			        } else if (idx.length === 1) {
						oTable_ref.getAggregation('items')[idx].getAggregation('cells')[0].setProperty('selected', false);
						oTable_ref.getAggregation('items')[parseInt(idx) - 1].getAggregation('cells')[0].setProperty('selected', true);
			        }
					
				}
				
				else if (evt.keyCode == 40 && (evt.ctrlKey)) { // down arrow
					
					var idx = []
					var oTable_ref = sap.ui.getCore().byId('oTable');
					for (var i in oTable_ref.getAggregation('items')) {
						if (oTable_ref.getAggregation('items')[i].getAggregation('cells')[0].getProperty('selected')) {
							idx.push(i);
						}
					}
					if (idx.length === 0) {
						oTable_ref.getAggregation('items')[0].getAggregation('cells')[0].setProperty('selected', true);
			        } else if (idx.length === 1) {
			        	oTable_ref.getAggregation('items')[idx].getAggregation('cells')[0].setProperty('selected', false);
						oTable_ref.getAggregation('items')[parseInt(idx) + 1].getAggregation('cells')[0].setProperty('selected', true);
					}
					
				}

				   


		});

	   },
	   


	   conceptDetailPage : function () {
		    $(document).unbind('keydown');
		    
		    $(document).keydown(function(evt){
				
				if (evt.keyCode == 8 && (evt.ctrlKey)) { // backspace
			       evt.preventDefault();
					sap.ui.controller("conceptmapper.conceptDetailPage").appBack();

			    }
		    });
		    

	   },
	   
	   reviewerPage : function () {
		    
		    $(document).unbind('keydown');
		    
		    
		    
	   },
	   
	   
	   mapperPage : function () {
		    
		    $(document).unbind('keydown');
	    	var oRelationTable = sap.ui.getCore().byId("relationTable");

		    
		    $(document).keydown(function(evt){
		    	
		    	if ((evt.keyCode == 38 || evt.keyCode == 40 ) && (evt.ctrlKey)) { // up or down
		    	
		    		sap.ui.getCore().byId("relationTable").getAggregation('items')[0].focus();
		    	}
				
		    	else if (evt.keyCode == 8 && (evt.ctrlKey) && !$("#searchFieldMapper-I").is(":focus") && !$("#oConceptMappingComment-inner").is(":focus")// backspace
		    			&& !$("#oVocMapperMCB-inner").is(":focus") && !$("#oDomainMapperMCB-inner").is(":focus") && !$("#oStandardMapperMCB-inner").is(":focus")) { 
				        evt.preventDefault();
						sap.ui.controller("conceptmapper.conceptDetailPage").appBack();

				}
				
		    	else if (evt.keyCode == 75 && (evt.ctrlKey) && !$("#searchFieldMapper-I").is(":focus")) { // k key
					evt.preventDefault();
					sap.ui.getCore().byId("searchFieldMapper").focus();

				}
		    	
		    	else if (evt.keyCode == 75 && (evt.ctrlKey) && $("#searchFieldMapper-I").is(":focus")) { // k key

					evt.preventDefault();
					$("#searchFieldMapper-I").blur();

				}
				
		    	else if ((evt.keyCode == 37 && (evt.ctrlKey) && sap.ui.getCore().byId('oButtonPrevConceptToMap').getEnabled()) ||
		    			(evt.keyCode == 37 && (evt.altKey) && sap.ui.getCore().byId('oButtonPrevConceptToMap').getEnabled())) { // left arrow
					sap.ui.controller("conceptmapper.mapperPage").newConcept('oEvt','prev');
					
				}
				
		    	else if ((evt.keyCode == 39 && (evt.ctrlKey) && sap.ui.getCore().byId('oButtonNextConceptToMap').getEnabled()) ||
	    			(evt.keyCode == 39 && (evt.altKey) && sap.ui.getCore().byId('oButtonNextConceptToMap').getEnabled())) { // right arrow

					sap.ui.controller("conceptmapper.mapperPage").newConcept('oEvt', 'next');
				}
		    	
				
		    	
		    	
		    	// MAP with LINE

		    	else if ((evt.keyCode == 97 || evt.keyCode == 49) && !$("#searchFieldMapper-I").is(":focus") && (evt.ctrlKey)) { 				// 1 -> 83
		    		var sId = oRelationTable.getAggregation('items')[0].getAggregation('cells')[0].getProperty('text');
					sap.ui.controller("conceptmapper.mapperPage").addRelation('oEvt', sId);

		    	}
		    	else if ((evt.keyCode == 98 || evt.keyCode == 50) && !$("#searchFieldMapper-I").is(":focus") && (evt.ctrlKey)){				// 2 -> 98
		    		var sId = oRelationTable.getAggregation('items')[1].getAggregation('cells')[0].getProperty('text');
					sap.ui.controller("conceptmapper.mapperPage").addRelation('oEvt', sId);

		    	}
		    	
		    	else if ((evt.keyCode == 99 || evt.keyCode == 51) && !$("#searchFieldMapper-I").is(":focus") && (evt.ctrlKey)){				// 3 -> 99

		    		var sId = oRelationTable.getAggregation('items')[2].getAggregation('cells')[0].getProperty('text');
					sap.ui.controller("conceptmapper.mapperPage").addRelation('oEvt', sId);

		    	}
		    	else if ((evt.keyCode == 100 || evt.keyCode == 52) && !$("#searchFieldMapper-I").is(":focus") && (evt.ctrlKey)){				// 4 -> 100

		    		var sId = oRelationTable.getAggregation('items')[3].getAggregation('cells')[0].getProperty('text');
					sap.ui.controller("conceptmapper.mapperPage").addRelation('oEvt', sId);

		    	}
		    	else if ((evt.keyCode == 101 || evt.keyCode == 53) && !$("#searchFieldMapper-I").is(":focus") && (evt.ctrlKey)){				// 5 -> 101

		    		var sId = oRelationTable.getAggregation('items')[4].getAggregation('cells')[0].getProperty('text');
					sap.ui.controller("conceptmapper.mapperPage").addRelation('oEvt', sId);

		    	}
		    	else if ((evt.keyCode == 102 || evt.keyCode == 54) && !$("#searchFieldMapper-I").is(":focus") && (evt.ctrlKey)){				// 6 -> 102

		    		var sId = oRelationTable.getAggregation('items')[5].getAggregation('cells')[0].getProperty('text');
					sap.ui.controller("conceptmapper.mapperPage").addRelation('oEvt', sId);

		    	}
		    	else if ((evt.keyCode == 103 || evt.keyCode == 55) && !$("#searchFieldMapper-I").is(":focus") && (evt.ctrlKey)){				// 7 -> 103

		    		var sId = oRelationTable.getAggregation('items')[6].getAggregation('cells')[0].getProperty('text');
					sap.ui.controller("conceptmapper.mapperPage").addRelation('oEvt', sId);

		    	}
		    	
		    	else if ((evt.keyCode == 104 || evt.keyCode == 56) && !$("#searchFieldMapper-I").is(":focus") && (evt.ctrlKey)){				// 8 -> 104

		    		var sId = oRelationTable.getAggregation('items')[7].getAggregation('cells')[0].getProperty('text');
					sap.ui.controller("conceptmapper.mapperPage").addRelation('oEvt', sId);

		    	}
		    	else if ((evt.keyCode == 105 || evt.keyCode == 57) && !$("#searchFieldMapper-I").is(":focus") && (evt.ctrlKey)){				// 9 -> 105
		    		var sId = oRelationTable.getAggregation('items')[8].getAggregation('cells')[0].getProperty('text');
					sap.ui.controller("conceptmapper.mapperPage").addRelation('oEvt', sId);

		    		
		    	}
		    	else if ((evt.keyCode == 96 || evt.keyCode == 48) && !$("#searchFieldMapper-I").is(":focus") && (evt.ctrlKey)){				// 0 -> 96

					sap.ui.controller("conceptmapper.mapperPage").addRelation('oEvt', '0');


		    	}



		    });
		    
	   }


   };
});