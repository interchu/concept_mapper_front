sap.ui.jsview("conceptmapper.searchPage", {

	/**
	 * Specifies the Controller belonging to this View. In the case that it is
	 * not implemented, or that "null" is returned, this View does not have a
	 * Controller.
	 * 
	 * @memberOf conceptmapper.searchPage
	 */
	getControllerName : function() {
		return "conceptmapper.searchPage";
	},

	/**
	 * Is initially called once after the Controller has been instantiated. It
	 * is the place where the UI is constructed. Since the Controller is given
	 * to this method, its event handlers can be attached right away.
	 * 
	 * @memberOf conceptmapper.searchPage
	 */
	createContent : function(oController) {
		

		

		/////////////// Master Page ///////////////
	
		var oMapperTitle = new sap.m.Title({
			width: "100%",
			text:"{i18n>mapper_filters}",
		});
		
		var oItemTemplateProject = new sap.ui.core.Item({
			key : "{m_project_type_id>key}",
			text : "{m_project_type_id>text}",
		});

		
		var oProjectMCB = new sap.m.MultiComboBox({
			id : "oTypeProject",
			width: "100%",
			placeholder : "{i18n>choose_project}",
			items : {
				path : "m_project_type_id>/items",
				template : oItemTemplateProject
			},
			selectedKeys : {
				path : "m_project_type_id>/selected",
				template : "{m_project_type_id>selected}"
			},
			selectionFinish: [ oController.searcher, oController ],

		});
		
		
		var oItemTemplateLang = new sap.ui.core.Item({
			key : "{m_language_id>key}",
			text : "{m_language_id>text}",
		});
		var oLangMCB = new sap.m.MultiComboBox({
			id : "oLangMCB",
			placeholder : "{i18n>language_label}",
			items : {
				path : "m_language_id>/items",
				template : oItemTemplateLang
			},
			selectedKeys : {
				path : "m_language_id>/selected",
				template : "{m_language_id>selected}"
			},
			selectionFinish: [ oController.searcher, oController ],

		});
		
		
		var oItemTemplateMap = new sap.ui.core.Item({
			key : "{is_mapped>key}",
			text : "{is_mapped>text}",
		});
		var oMapMCB = new sap.m.MultiComboBox({
			id : "oMapMCB",
			placeholder : "{i18n>mapping_label}",
			items : {
				path : "is_mapped>/items",
				template : oItemTemplateMap
			},
			selectedKeys : {
				path : "is_mapped>/selected",
				template : "{is_mapped>selected}"
			},
			
			selectionFinish: [ oController.searcher, oController ],

		});
		
		
		
		
		
		
		
		var omop_title = new sap.m.Title({
			width: "100%",
			text:"{i18n>omop_filters}",


		});
		
		var oMiniIB = new sap.m.Input({
			id : "oMiniIB",
			placeholder : "{i18n>mini_id}",

		});

		var oMaxiIB = new sap.m.Input({
			id : "oMaxiIB",
			placeholder : "{i18n>maxi_id}",
		});
		
		var oItemTemplateStandard = new sap.ui.core.Item({
			key : "{standard_concept>key}",
			text : "{standard_concept>text}",
			enabled : "{standard_concept>enabled}"
		});
		var oStandardMCB = new sap.m.MultiComboBox({
			id : "oStandardMCB",
			placeholder : "{i18n>standard_label}",
			items : {
				path : "standard_concept>/items",
				template : oItemTemplateStandard
			},
			selectedKeys : {
				path : "standard_concept>/selected",
				template : "{standard_concept>selected}"
			},
			selectionFinish: [ oController.searcher, oController ],

		});
		
		var oItemTemplateValid = new sap.ui.core.Item({
			key : "{invalid_reason>key}",
			text : "{invalid_reason>text}",
			enabled : "{invalid_reason>enabled}"
		});
		var oValidMCB = new sap.m.MultiComboBox({
			id : "oValidMCB",
			placeholder : "{i18n>valid_label}",
			items : {
				path : "invalid_reason>/items",
				template : oItemTemplateValid
			},
			selectedKeys : {
				path : "invalid_reason>/selected",
				template : "{invalid_reason>selected}"
			},
			selectionFinish: [ oController.searcher, oController ],

		});
		

		var oItemTemplateDomain = new sap.ui.core.Item({
			key : "{domain_id>key}",
			text : "{domain_id>text}",
			enabled : "{domain_id>enabled}"
		});
		var oDomainMCB = new sap.m.MultiComboBox({
			id : "oDomainMCB",
			placeholder : "{i18n>domain_label}",
			items : {
				path : "domain_id>/items",
				template : oItemTemplateDomain
			},
			selectedKeys : {
				path : "domain_id>/selected",
				template : "{domain_id>selected}"
			},
			selectionFinish: [ oController.searcher, oController ],

		});

		var oItemTemplateVoc = new sap.ui.core.Item({
			key : "{vocabulary_id>key}",
			text : "{vocabulary_id>text}",
			enabled : "{vocabulary_id>enabled}"
		});
		var oVocMCB = new sap.m.MultiComboBox({
			id : "oVocMCB",
			placeholder : "{i18n>vocabulary_label}",
			items : {
				path : "vocabulary_id>/items",
				template : oItemTemplateVoc
			},
			selectedKeys : {
				path : "vocabulary_id>/selected",
				template : "{vocabulary_id>selected}"
			},
			selectionFinish: [ oController.searcher, oController ],

		});


		
	
		var oMasterPage = new sap.m.Page({
			title : "{i18n>filters}",
			showNavButton : true,

			content : [ oMapperTitle,  oProjectMCB, oLangMCB, oMapMCB , 
				omop_title, oMiniIB, oMaxiIB,  oDomainMCB, oVocMCB, oStandardMCB, oValidMCB,
					 
					],

			
		});

		
		
		
		
		
		
		
		
		
		
		// Detail Page		

		var aColumns = [
			new sap.m.Column({
				id: "toMapSearch",
				header : new sap.m.Label({
					text : "{i18n>to_map}"
				})
			
			}),
			new sap.m.Column({
				id: "conceptIdSearch",
				header : new sap.m.Label({
					text : "{i18n>concept_id}"
				}),

			}),
			new sap.m.Column({
				id: "conceptNameSearch",
				header : new sap.m.Label({
					text : "{i18n>concept_name}"
				}),

			}),
			new sap.m.Column({
				id: "conceptCodeSearch",
				header : new sap.m.Label({
					text : "{i18n>concept_code}"
				}),
				visible: false,
			}),
			new sap.m.Column({
				id: "conceptClassSearch",
				header : new sap.m.Label({
					text : "{i18n>concept_class_id}"
				}),
				visible: false,

			}),
			new sap.m.Column({
				id: "vocabularySearch",
				header : new sap.m.Text({
					text : "{i18n>vocabulary_id}"
				}),
			}),
			new sap.m.Column({
				id: "domainSearch",

				header : new sap.m.Label({
					text : "{i18n>domain_id}"
				}),
			}),
			new sap.m.Column({
				id: "standardSearch",
				header : new sap.m.Label({
					text : "{i18n>standard_concept}"
				}),
				visible: false,

			}),
			new sap.m.Column({
				id: "validitySearch",
				header : new sap.m.Label({
					text : "{i18n>validity}"
				}),
				visible: false,

			}),
			new sap.m.Column({
				id: "frequencySearch",

				header : new sap.m.Label({
					text : "{i18n>frequency}"
				}),
				visible: false,

			}),

			
			

		];
		
		
		


		
		

		var selectedItems = []
		
		var oTemplate = new sap.m.ColumnListItem({

			type: "Active",
			highlight:"{conceptsModel>status}",
			press : [ oController.goToConceptDetail, oController ],
			
			cells : [
				new sap.m.CheckBox({				
				}),
				new sap.m.Text({
					text : "{conceptsModel>concept_id}",
					wrapping : false,
				}),
				new sap.m.FormattedText({
					htmlText : "{conceptsModel>concept_name}",
				}),
				new sap.m.Text({
					text : "{conceptsModel>concept_code}",
					wrapping : false,
				}),
				new sap.m.Text({
					text : "{conceptsModel>concept_class_id}",
					wrapping : false,
				}),
				
				new sap.m.Text({
					text : "{conceptsModel>vocabulary_id}",
					wrapping : false,
				}),
				new sap.m.Text({
					text : "{conceptsModel>domain_id}",
					wrapping : false,
				}),
				new sap.m.Text({
					text : "{conceptsModel>standard_concept}",
					wrapping : false,
				}),
				new sap.m.Text({
					text : "{conceptsModel>invalid_reason}",
					wrapping : false,
				}),
				
				new sap.m.Text({
					text : "{conceptsModel>frequency}",
					wrapping : false,
				}),
				
				
			]
		});

		
		var oTable = new sap.m.Table({
			id : "oTable",
			fixedLayout:false,
			
			columns : aColumns,

		});

		
		
		
		oTable.bindItems({
			path : "conceptsModel>/docs",

			template : oTemplate,
			key: "id"
		});
		
		
		
		
		
		
		
		
		
		
		
		
		
		
		
		
		
		
		
		
		
		
		
		
//		
//		var oSubHeader = new sap.m.Bar({
//			contentMiddle : [ new sap.m.Label({
//				text : "{i18n>app_subhead_search}"
//			}), 
//			new sap.m.Button({
//				icon: "sap-icon://sort"
//			})
//			]
//		});
//	
		

		
		var oToolbarConceptsNumber = new sap.m.Toolbar({
			height: "4rem",
			content : [

				new sap.m.ToolbarSpacer(),
				new sap.m.Label({
					id:"numFound",
					text:"{conceptsModel>/num_found}",
					visible:false
				}).addStyleClass('conceptNumFound'),
				new sap.m.Button({
					text:"{i18n>concepts_found}",
					enabled:false,
					type: sap.m.ButtonType.Transparent,
				}),

			]
		}).addStyleClass('borderBottom');
		
		
		var oRowsPerLignTemplate = new sap.ui.core.Item({
			key : "{RowsPerLignModel>key}",
			text : "{RowsPerLignModel>text}",
			enabled : "{RowsPerLignModel>enabled}"
		});
		
		var oToolbarControls = new sap.m.Toolbar({
			content : [ 
				

				
				
                new sap.m.Button({
                	id: "hideShowFilter", 
					icon: "sap-icon://filter",
//                	tooltip: "{i18n>filters}", 
					type: "Transparent",
					enabled:false,
					type: "Transparent",
                	press: function(){
     		    	    sap.ui.getCore().byId("__container1-Master").toggleStyleClass("toggle-masterPage");
                	}
                }),
				new sap.m.Button({
                	id: "sortSearchPage", 
					icon: "sap-icon://sort",
					type: "Transparent",
					enabled:false,
					press: [oController.openSortSPFragment, oController],
				}),
                
				new sap.m.Button({
                	id: "customizeSearchPage", 
					icon: "sap-icon://customize",
                	tooltip: "{i18n>select_columns}", 
					type: "Transparent",
                	enabled:false,
					press: [oController.openCustomizeSPFragment, oController],
				}),
				new sap.m.ToolbarSpacer(),

					
				new sap.m.SearchField({
					id: 'searchField',
					placeholder : "{i18n>research}",
					enabled:false,
					search : [ oController.searcher, oController ],
				}).addStyleClass('searchField'),
				new sap.m.ToolbarSpacer(),

		 
				new sap.m.ComboBox({
					id: "oRowInPageInput",
					enabled:false,
					items : {
						path : "RowsPerLignModel>/items",
						template : oRowsPerLignTemplate
					},
					selectedKey : {
						path : "RowsPerLignModel>/selected",
						template : "{RowsPerLignModel>selected}"
					}
				}).addStyleClass('rowInPageInput'),
				new sap.m.Button({
					text : "{i18n>config_items_per_page}",
					enabled: false,
					type: sap.m.ButtonType.Transparent,

				}),
			
			]
		});

		
		
		var oDetailPage = new sap.m.Page({
//			title : "{i18n>app_head}",
			showNavButton : false,
			navButtonPress : function(oEvt) {
				app.back();
			},
			customHeader : new sap.m.Bar({
                contentLeft: new sap.m.Button({
					icon : "sap-icon://home-share",
					text: "Git",
					press: function() {
						window.open('https://framagit.org/interchu/conceptual-mapping');
					}
				}), 
				contentMiddle: new sap.m.Title({
					text: "{i18n>app_head}",
				}), 
                contentRight: [
				
                	new sap.m.Button({
    					id: 'usersParam',
    					icon : "sap-icon://collaborate",
    					visible: false,
    					press: [ oController.goToUserPage, oController ],
    		
    				}),
				
					new sap.m.Button({
						id: 'registrationSearch',
						icon : "sap-icon://employee",
						type: "Accept",
						press: [ oController.userAuthentification, oController ],
			
					}),
					new sap.m.Button({
						id: 'loginSearch',
						text: '{loginModel>/m_username}',
						icon : "sap-icon://employee",
						enabled:true,
						visible: false,
	
						press: [ oController.userInfos, oController ],
					}),
					new sap.m.Button({
						id: 'logoutSearch',
						text: 'Log-out',
						icon : "sap-icon://decline",
						visible: false,
	
						press: [ oController.logout, oController ],
				})
			]}),
			content : [oToolbarConceptsNumber, oToolbarControls,  oTable ],
			
			footer: new sap.m.Bar({
                contentLeft: [
    				new sap.m.Button({
                    	id: "mapper", 
                    	text: "{i18n>map_button}", 
    					type : "Reject",
    					enabled:false,
                    	press: [ oController.goToMapperPage, oController ],
    				}),
				
                ],
				
				
        		contentMiddle: [
   
            		
            		
            		new sap.m.SearchField({
    					id: 'oSearchFielGoToPage',
    					enabled:false,
            			placeholder : "{i18n>go_to_page}",
    					enabled:false,
    					search : [ oController.searcherGoToPage, oController ],
    				}).addStyleClass('goTo'),
			
        			new sap.m.Button({
            			id : "oButtonCurrentPage",
                        text: "{i18n>current_page}" ,
                        enabled:false,
                	}), 
        			new sap.m.Button({
	            		id : "oButtonPrev",
	                	text: "{i18n>previous}",
						type : "Transparent",
						enabled: false,
						icon : "sap-icon://navigation-left-arrow",
    					press : [ oController.prevPage, oController ],
                		
                	}),  
                	new sap.m.Button({
	            		id : "oButtonNext",
	                	text: "{i18n>next}",
    					type : "Transparent",
						enabled: false,
    					iconFirst: false,
    					icon : "sap-icon://navigation-right-arrow",
    					press : [ oController.nextPage, oController ],
                	}),
	                new sap.m.Button({
            			id : "oButtonTotalPages",
	                    text: "{i18n>total_pages}",
	                    enabled:false,
	            	}), 
            	
                ],
                
                contentRight: [
   
            		
            		
                	 new sap.m.Button({
             			id : "switchToReviewer",
    					icon : "sap-icon://journey-change",
    					type : "Emphasized",
                    	tooltip: "{i18n>switch_to_mapping-reviewer}", 
 	                    enabled:false,
 	                    press:  [ oController.goToReviewerMapper, oController ]
                	 
 	            	}), 
 	            
// 	            	new y.Hmello({name:"UI5"}),
 	            	
// 	            	new HoverButton("myBtn", {
// 	            	      text: "Hover Me",
// 	            	      hover: function(evt) {
// 	            	          alert("Button " + evt.getSource().getId() + " was hovered.");
// 	            	      }
// 	            	  })
                ]
        		
        		

			})
		});
		
		
		
		/////////////// All in one Page ///////////////

		var oSplitContainer = new sap.m.SplitContainer({mode: sap.m.SplitAppMode.ShowHideMode});
		oSplitContainer.addMasterPage(oMasterPage);
		oSplitContainer.addDetailPage(oDetailPage);
		

		return oSplitContainer;
	}

});