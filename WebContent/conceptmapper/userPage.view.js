sap.ui.jsview("conceptmapper.userPage", {

	/**
	 * Specifies the Controller belonging to this View. In the case that it is
	 * not implemented, or that "null" is returned, this View does not have a
	 * Controller.
	 * 
	 * @memberOf conceptmapper.userPage
	 */
	getControllerName : function() {
		return "conceptmapper.userPage";
	},

	/**
	 * Is initially called once after the Controller has been instantiated. It
	 * is the place where the UI is constructed. Since the Controller is given
	 * to this method, its event handlers can be attached right away.
	 * 
	 * @memberOf conceptmapper.userPage
	 */
	
	createContent : function(oController) {
		
		
		var oItemTemplateProjects = new sap.ui.core.Item({
			key : "{availableProjects>key}",
			text : "{availableProjects>text}",
		});
		
		var oTable = new sap.ui.table.Table('oUserTable', {
			toolbar : new sap.m.Toolbar({
				content : [ new sap.m.Button({
					text : "{i18n>create_user}",
					type : "Accept",
					press : [ oController.openCreateDialog, oController ],
				}), new sap.m.Button({
					text : "{i18n>update_user}",
					type : "Reject",
					press : [ oController.openUpdateDialog, oController ],
				}), 

				]
			}),
		});

		oTable.addColumn(new sap.ui.table.Column({
			label : new sap.ui.commons.Label({
				text : "{i18n>username}" 
			}),
			template : new sap.m.Text({
				text : "{usersModel>m_username}"
			}),
			sortProperty : "m_username"
		}));

		oTable.addColumn(new sap.ui.table.Column({
			label : new sap.ui.commons.Label({
				text : "{i18n>status}"
			}),
			template : new sap.m.Text({
				text : "{usersModel>m_invalid_reason}"
			}),
			sortProperty : "m_invalid_reason",

		}));
		
		oTable.addColumn(new sap.ui.table.Column({
			label : new sap.ui.commons.Label({
				text : "{i18n>is_admin}"
			}),
			template : new sap.m.Text({
				text : "{usersModel>m_is_admin}"
			}),
			sortProperty : "m_is_admin",
		}));
		

		oTable.addColumn(new sap.ui.table.Column({
			label : new sap.ui.commons.Label({
				text : "{i18n>firstname}"
			}),
			template : new sap.m.Text({
				text : "{usersModel>m_firstname}"
			}),
			sortProperty : "m_firstname",
		}));

		oTable.addColumn(new sap.ui.table.Column({
			label : new sap.ui.commons.Label({
				text : "{i18n>lastname}"
			}),
			template : new sap.m.Text({
				text : "{usersModel>m_lastname}"
			}),
			sortProperty : "m_lastname",
		}));

		oTable.addColumn(new sap.ui.table.Column({
			label : new sap.ui.commons.Label({
				text : "{i18n>address}"
			}),
			template : new sap.m.Text({
				text : "{usersModel>m_address}"
			}),
			sortProperty : "m_address",
		}));

		oTable.addColumn(new sap.ui.table.Column({
			label : new sap.ui.commons.Label({
				text : "{i18n>email}"
			}),
			template : new sap.m.Text({
				text : "{usersModel>m_email}"
			}),
			sortProperty : "m_email",

		}));
		
		oTable.addColumn(new sap.ui.table.Column({
			label : new sap.ui.commons.Label({
				text : "Projets" 
			}),
			template : new sap.m.MultiComboBox({
				width: "100%",
				placeholder : "{i18n>choose_project}",
				items : {
					path : "availableProjects>/items",
					template : oItemTemplateProjects
				},
				selectedKeys : "{usersModel>m_project_role}",

				selectionFinish: [ oController.changeProject, oController ],

			}),

		}));

		oTable.bindRows({
			path : "usersModel>/users"
		});

		
		
		
		var oSubHeader = new sap.m.Bar({

			contentMiddle : [ new sap.m.Label({
				text : "{i18n>app_subhead_csv}"
			}) ]
		});
		
		var oPage = new sap.m.Page({
			title : "{i18n>app_head}",
			showNavButton: true, 
			navButtonPress : function(oEvt) {
				app.back();
			},
			showSubHeader : true,
			subHeader : oSubHeader,

			content : [ oTable ]
		});

		return oPage;
	}

});