sap.ui.jsview("conceptmapper.CSVPage", {

	/**
	 * Specifies the Controller belonging to this View. In the case that it is
	 * not implemented, or that "null" is returned, this View does not have a
	 * Controller.
	 * 
	 * @memberOf conceptmapper.CSVPage
	 */ 
	getControllerName : function() {
		return "conceptmapper.CSVPage";
	},

	/**
	 * Is initially called once after the Controller has been instantiated. It
	 * is the place where the UI is constructed. Since the Controller is given
	 * to this method, its event handlers can be attached right away.
	 * 
	 * @memberOf conceptmapper.CSVPage
	 */ 
	createContent : function(oController) {
		var oSubHeader = new sap.m.Bar({
//			contentLeft : [ new sap.m.Button({
//				icon : "sap-icon://nav-back",
//				press : function(oEvt) {
//					app.back();
//				}
//			}) ],
			contentMiddle : [ new sap.m.Label({
				text : "Upload CSV"
			}) ]
		});

		var oUpload = new sap.ui.unified.FileUploader().addStyleClass("CSVPage");
		var oButton = new sap.m.Button({
			text:"{i18n>CSV_upload}"
		}).addStyleClass("CSVPage");

		var oPage = new sap.m.Page({
			title : "{i18n>app_head}",
			showNavButton: true, //peut etre que c'est mieux de le mettre ici
			navButtonPress : function(oEvt) {
				app.back();
			},
			showSubHeader : true,
			subHeader : oSubHeader,
			headerContent : [
				new sap.m.Button({
					// text:"lol", 
					icon:"sap-icon://account"
				})
			],
			content : [ oUpload, oButton ]
		});


		return oPage;
	}

});