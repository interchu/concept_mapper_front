sap.ui.define([
	"sap/ui/core/mvc/Controller",
	"./utils/userAjax",
		], function(Controller, userAjax) {
		   "use strict";

return Controller.extend("conceptmapper.userInfosPage", {

	
	resetPwd: function() {
		
		var oData = {};
		
		var sUsername = sap.ui.getCore().byId('modifUsername').getProperty('value');
		oData.m_firstname = sap.ui.getCore().byId('modifFirstname').getProperty('value');
		oData.m_lastname = sap.ui.getCore().byId('modifLastname').getProperty('value');
		oData.m_address = sap.ui.getCore().byId('modifAddress').getProperty('value');
		oData.m_email = sap.ui.getCore().byId('modifEmail').getProperty('value');
		if (sap.ui.getCore().byId('modifPassword').getProperty('value') !== "") {
			oData.m_password = sap.ui.getCore().byId('modifPassword').getProperty('value');
			var confirmPassword = sap.ui.getCore().byId('modifConfirmPassword').getProperty('value');
		}
		
		debugger;
		
		if (sUsername !== "" && oData.m_firstname !== "" && oData.m_lastname !== ""  
			&& oData.m_address !== ""  && oData.m_email !== "" && 
			( (sap.ui.getCore().byId('modifPassword').getProperty('value') === "" && 
					sap.ui.getCore().byId('modifConfirmPassword').getProperty('value') === "") 
				|| (oData.m_password && oData.m_password !== "" && oData.m_password === confirmPassword ) )) {
			userAjax.updateUserAjax(oData, sUsername);
			app.back();
			
		} else if (!oData.m_password || !confirmPassword || oData.m_password !== confirmPassword) {
			sap.m.MessageToast.show("Not the same password");
		} else {
			sap.m.MessageToast.show("Try again");
		}
		
	},
	
	showHidePassword: function(oEvt, sController) {
		
		if (sController === "hide") {
			sap.ui.getCore().byId('modifPassword').setVisible(false);
			sap.ui.getCore().byId('modifConfirmPassword').setVisible(false);
			sap.ui.getCore().byId('modifPassword').resetProperty("value");
			sap.ui.getCore().byId('modifConfirmPassword').resetProperty("value");

		} else if (sController === "show") {
			sap.ui.getCore().byId('modifPassword').setVisible(true);
			sap.ui.getCore().byId('modifConfirmPassword').setVisible(true);

		}
		
	},


/**
* Called when a controller is instantiated and its View controls (if available) are already created.
* Can be used to modify the View before it is displayed, to bind event handlers and do other one-time initialization.
* @memberOf conceptmapper.userInfos
*/
//	onInit: function() {
//
//	},

/**
* Similar to onAfterRendering, but this hook is invoked before the controller's View is re-rendered
* (NOT before the first rendering! onInit() is used for that one!).
* @memberOf conceptmapper.userInfos
*/
//	onBeforeRendering: function() {
//
//	},

/**
* Called when the View has been rendered (so its HTML is part of the document). Post-rendering manipulations of the HTML could be done here.
* This hook is the same one that SAPUI5 controls get after being rendered.
* @memberOf conceptmapper.userInfos
*/
//	onAfterRendering: function() {
//
//	},

/**
* Called when the Controller is destroyed. Use this one to free resources and finalize activities.
* @memberOf conceptmapper.userInfos
*/
//	onExit: function() {
//
//	}

});
});